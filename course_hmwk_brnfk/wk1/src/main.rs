use std::env::args;
use std::error::Error;
use std::fs::File;
use std::io::prelude::*;
use std::io::BufReader;
use std::path::PathBuf;

const BRN_FK_CHARS: [char; 8] = ['<', '>', '+', '-', '.', ',', '[', ']'];

fn main() -> Result<(), Box<dyn Error>> {
    let source_filename: String = args()
        .nth(1)
        .ok_or("Missing filename: please provide a filename")?;
    let f = File::open(PathBuf::from(&source_filename))
        .map_err(|e| format!("Problem opening {}: {}", &source_filename, e))?;
    let file_reader = BufReader::new(f);

    let valid_lines: Result<Vec<String>, std::io::Error> = file_reader.lines().collect();
    let valid_lines: Vec<String> = valid_lines?;

    let valid_chars: Vec<char> = valid_lines
        .iter()
        .flat_map(|ln| ln.chars())
        .filter(|c| BRN_FK_CHARS.contains(c))
        .collect();

    for character in valid_chars {
        print!("{}", character);
    }
    println!();
    Ok(())
}
