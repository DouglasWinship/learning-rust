use std::env::args;
use std::error::Error;
use std::fs::File;
use std::io::prelude::*;
use std::io::BufReader;
use std::path::PathBuf;

fn main() -> Result<(), Box<dyn Error>> {
    let source_filename: String = args()
        .nth(1)
        .ok_or("Missing filename: please provide a filename")?;
    let f = File::open(PathBuf::from(&source_filename))
        .map_err(|e| format!("Problem opening {}: {}", &source_filename, e))?;
    let file_reader = BufReader::new(f);

    let final_total: Result<f64, Box<dyn Error>> = file_reader
        .lines()
        .try_fold(0.0, |running_total: f64, line| {
            Ok(running_total + line?.parse::<f64>()?)
        });
    let final_total = final_total?;

    println!("Final total:{}", final_total);
    Ok(())
}
